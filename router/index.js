const express = require("express");
const router = express.Router();

router.get("/", (req, res) => {
	const params = {
		nContador: req.query.nContador,
		nombre: req.query.nombre,
		domicilio: req.query.domicilio,
		niEstudio: req.query.niEstudio,
		pDiaBase: parseInt(req.query.pDiaBase),
		dTraba: parseFloat(req.query.dTraba),
	};

	res.render("index.html", params);
});

router.post("/", (req, res) => {
	const params = {
		nContador: req.body.nContador,
		nombre: req.body.nombre,
		domicilio: req.body.domicilio,
		niEstudio: req.body.niEstudio,
		pDiaBase: parseInt(req.body.pDiaBase),
		dTraba: parseFloat(req.body.dTraba)
	};
});

router.get("/mostrar", (req, res) => {
	const params = {
		nContador: req.query.nContador,
		nombre: req.query.nombre,
		domicilio: req.query.domicilio,
		niEstudio: req.query.niEstudio,
		pDiaBase: parseInt(req.query.pDiaBase),
		dTraba: parseFloat(req.query.dTraba),
	};

	res.render("mostrar.html", params);
});

router.post("/mostrar", (req, res) => {
	const params = {
		nContador: req.body.nContador,
		nombre: req.body.nombre,
		domicilio: req.body.domicilio,
		niEstudio: req.body.niEstudio,
		pDiaBase: parseInt(req.body.pDiaBase),
		dTraba: parseFloat(req.body.dTraba)
	};
	res.render("mostrar.html", params);
});

module.exports = router;